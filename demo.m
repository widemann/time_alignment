%% Processing data for time alignment. 

% The steps belows should walk you though my approach to time aligning a
% subset of the spoken words. The main idea is this, first use the prosody
% tool developed here http://prosodylab.org/tools/aligner/ to roughly align
% the data. There is no need to download the code, it's attached. 
% Then we use our technique to determine the beginning of a 
% for only a subset of the words.

% The final output is a cell with:
% column 1 = word (prosody format)
% column 2 = word (Suppe lab format)
% column 3 = start time (Suppe lab)
% column 4 = start time (prosody)
% column 5 = end time (prosody)
% column 6 = start time (only for subset of words)

% The idea is to compare the non-nans in column 6 with those in column 3.
% The results are in /Prosodylab-Aligner-master/results_test

%% Step 1
% install the data for Couple A_01_25_11 under this directory so that the
% their data can be read-in. 
pth = pwd;
addpath(genpath(pth));

%% Step 2 readin some of the audio files. (you can change this to read in as
% much as you want.)
writeToDirec = 'Prosodylab-Aligner-master/results';
mkdir(writeToDirec);
direc = 'Couple A_01_25_11/sound';
cd(direc)
wavFiles = dir('*.wav');
samps = [1,9600000]; % only do this for the first 10 minutes of data. 
sigs = [];
for k = 1:numel(wavFiles)
    %[y, fs, nbits] = wavread(fullfile(direc,wavFiles(k).name));
    [y, fs, nbits] = wavread(fullfile(direc,wavFiles(k).name),samps);
    sigs = [sigs y];
end
cd(pth)
N = size(sigs,1);
dt = 1/fs;
t = dt*(0:N-1); 
cd(pth);

%% Step 3 combine signals based on variance to create one signal.
varWidth = 1/10;  % seconds
varSamp = fs*varWidth; % number of samples
bxCar = (1/varSamp)*ones(varSamp,1);
avg = filter(bxCar,1,sigs);
variance = filter(bxCar,1,(sigs-avg).^2)';
% variance2 = filter(bxCar,1,tmp.^2) - avg.^2;
[v, speakerInds] = max(variance);
sig = zeros(N,1);
for k = 1:N
    sig(k) = sigs(k,speakerInds(k));
end

% figure, plot(t,sig), hold on

%% Step 4 Every t_step seconds cut the wav file based on variance.
varWidth = 1/10;  % seconds
varSamp = fs*varWidth; % number of samples
bxCar = (1/varSamp)*ones(varSamp,1);
avg_sig = filter(bxCar,1,sig);
var_sig = filter(bxCar,1,(sig - avg_sig).^2);
t_step = 5;
Tf = N/fs;
time_cuts = [];
win = 3;
ts = t_step;
while ts < Tf
    inds = (t >= ts-win & t < ts+win);
    [a, v] = min(var_sig(inds));
    tc = ts-win + v/fs;
    time_cuts = [time_cuts; tc];
    ts = tc + t_step;
end
save(fullfile(writeToDirec,'time_cuts.mat'),'time_cuts');

% % if you want to see what the code is doing run the graph below. 
% for k = 1:numel(time_cuts)
%    inds = t >= time_cuts(k)-win & t < time_cuts(k)+win; 
%    figure, plot(t(inds),sig(inds)), hold on
%    m = max(abs(sig(inds)));
%    line([time_cuts(k) time_cuts(k)],[-m m],'color','r') 
%    pause;
%    close
% end

%% Step 5 create a 8 KHz .wav file for the same time period. I find that prosody
% works best at 8 KHz. 
fs_8 = 8e3;
sig_8kHz = resample(sig,1,fs/fs_8);
tnew = (1/fs_8)*(0:numel(sig_8kHz)-1);
tmp = wavFiles(1).name;
v = strfind(tmp,'_mono');
name = tmp(1:v(1));
k = 1;
fn = sprintf('%s%d',name,k);
inds = tnew >= 0 & tnew < time_cuts(k);
wavwrite(sig_8kHz(inds),fs_8,fullfile(writeToDirec,fn));
for k = 1:numel(time_cuts)-1
   fn = sprintf('%s%d',name,k+1);
   inds = tnew >= time_cuts(k) & tnew < time_cuts(k+1);
   wavwrite(sig_8kHz(inds),fs_8,fullfile(writeToDirec,fn));
end
k=k+1;
inds = tnew >= time_cuts(k) & tnew < Tf;
fn = sprintf('%s%d',name,k+1);
wavwrite(sig_8kHz(inds),fs_8,fullfile(writeToDirec,fn));

%% Step 6
% Create a .lab file for each time period. The assumption behind doing this
% is that whoever is transcribing the data will do so in a manner that
% creates a transcription file for approximately every 10 seconds of audio. 
load('Couple A_01_25_11/transcript/01_25_11.mat');
mil = 1e3;
k = 1;
fn = sprintf('%s%d.lab',name,k);
eventTimes = cell2mat(EventsData(:,1))/mil;
inds = eventTimes >= 0 & eventTimes < time_cuts(k);
cell_str = upper(EventsData(inds,3));
create_lab_file_fcn(fullfile(writeToDirec,fn),cell_str);
for k = 1:numel(time_cuts)-1
   fn = sprintf('%s%d.lab',name,k+1);
   inds = eventTimes >= time_cuts(k) & eventTimes < time_cuts(k+1);
   cell_str = upper(EventsData(inds,3));
   tmp = fullfile(writeToDirec,fn);
   create_lab_file_fcn(tmp,cell_str);
end
k=k+1;
inds = find(eventTimes >= time_cuts(k) & eventTimes < Tf);
fn = sprintf('%s%d.lab',name,k+1);
cell_str = upper(EventsData(inds,3));
tmp = fullfile(writeToDirec,fn);
create_lab_file_fcn(tmp,cell_str);
cd(pth)

%% Step 7 Run prosodylab. Note, HTK, SoX and python must be installed in order to
% run this software. If for some reason the command does not work, follow
% the instructions in the README.md file of prosodylab to make sure you have
% everything installed. Note, there are some words in the audio
% transcriptions, that were not in the prosody dictionary. It's easy to add
% them. I did this for this data set. See the prosody README.md file to see
% how this is done.
% I could not get this to work from within matlab. Go to Prosodylab-Aligner-master/ 
% from within command line prompt and run:

% ./align.py -d modified_dictionary.txt results

% This should create the .TextGrid files with the results in them.

%% Step 7 Store the output of the trained Prosody w/ the results from the lab. 
% It's a cell structure with first column being from the .lab file, the
% second column being from EventData, the third column is the start time as 
% in EventData, columns 4 and 5 are the start time and end time of the
% word as given by prosody. 
numTimeCuts = numel(time_cuts);
% read in the text grid file 
cd(writeToDirec);
textGridFiles = dir('*.TextGrid');
% create a cell that contains the word it's event file time and Prosody
% time start and finish
R = cell(0);
cnt = 0;
for k = 1:numel(textGridFiles)
    s = textGridFiles(k).name;
    v = find(s=='.');
    w = find(s=='_');
    num = str2num(s(w(2)+1:v-1));
    [phoneme, word] = read_textgrid(textGridFiles(k).name);
    if num == 1
        t0 = 0;
        tf = time_cuts(num);
    elseif num <= numTimeCuts
        t0 = time_cuts(num-1);
        tf = time_cuts(num);
    else
        t0 = time_cuts(end);
        tf = inf;
    end  
    indsTime = eventTimes >= t0 & eventTimes < tf;
    if ~isempty(indsTime)
        inds = logical(indsTime);
    else
        inds = [];
    end
    A = EventsData(inds,:);
    % for each word find it's place in the event file
    if numel(word) > 1
        for j = 1:numel(word)
            pt = []; % potential times
            for m = 1:size(A,1)
                pat = '[A-Z'']';
                str = upper(A{m,3});
                ss = regexp(str,pat);
                if ~isempty(ss)
                    tmp_s = str(ss);
                    w = find(tmp_s=='''');
                    if (w ~= length(tmp_s)-1) | (w ~= length(tmp_s)-1)
                        tmp_s(w) = [];
                    end
                    if strcmpi(tmp_s,'UNCLEAR')
                        tmp_s = '';
                    end   
                end
                if strcmpi(tmp_s,word(j).text)
                    pt = [pt; A{m,1}/mil];
                end
            end
            if ~isempty(pt)
               [mm idx] = min(abs(pt - t0 + word(j).xmin));
               cnt = cnt+1;
               R{cnt,1} = word(j).text;
               vv = find((cell2mat(A(:,1))/mil)==pt(idx));
               R{cnt,2} = A{vv,3};
               R{cnt,3} = pt(idx);
               R{cnt,4} = t0 + word(j).xmin;
               R{cnt,5} = t0 + word(j).xmax;   
            end
        end
    end
    fprintf('%s done\n',textGridFiles(k).name);
end

[v,idx] = sort(cell2mat(R(:,3)),'ascend');
output = R(idx,:);
save output.mat output

%% Step 8 use the sig generated in step 2 above to determine which words 
% have good start times, i.e. fit our description of easily detectable. 
doPlot = 1;
% playAudio = 1;
load output
% find a segment of audio w/ mostly noise
noiseTimes = [0,10]; % seconds
inds = t >= noiseTimes(1) & t < noiseTimes(2);
avgFilter = 20;
bxCar = (1/avgFilter)*ones(1,avgFilter);
smoothed = filter(bxCar,1,sig);
noiseMean = mean(smoothed(inds));
noiseSig = std(smoothed(inds));
numSig = 4;
q = 0.25; % make sure everything in the first q percent of window is less than thresh.
thresh = noiseMean + numSig*noiseSig;
N = size(output,1);
I = randperm(N); %1:N; 
win = .1; % seconds, how far to look before prosody says the start time. 
d = [1 -1]; % second derivative
for k = 1:N
    newStart = nan;
    prosodyTime = output{I(k),4};
    t0 = prosodyTime-win;
    tf = win + prosodyTime;
    inds = find(t >= t0 & t < tf);
    preInds = t >= t0 & t < t0+q*win;
    % make sure everything in the first q of the signal is less than thresh
    v = (abs(sig(preInds)) < thresh);
    output{I(k),6} = newStart;
    if sum(preInds) == sum(v) 
        % find the first point that the signal is above the threshold
        v = find(abs(smoothed(inds)) > thresh,1);
        if ~isempty(v)
            % find the point before in which it's not monotonically increasing.
            % yes, this isn't the best thing to do but it's a start. 
            newInds = inds(1):inds(1)+v-1;
            tt = t(newInds);
            newSig = abs(smoothed(newInds));
            d_newSig = filter(d,1,newSig);
            sd_newSig = sign(d_newSig);
            %figure, plot(tt,newSig,tt,d_newSig)
            a = find(sd_newSig==-1,1,'last');
            if ~isempty(a)
                newStart = (tt(a) + tt(a+1))/2;
                output{I(k),6} = newStart;
                if doPlot == 1
        %             tmp = thresh*ones(size(sig(inds)));
                    figure, plot(t(inds),sigs(inds,:)), hold on
                    title(output{I(k),1})
                    mm = max(max(abs(sigs(inds,:))));
                    line([output{I(k),3},output{I(k),3}],[-mm mm],'color','m')
                    line([newStart newStart],[-mm mm],'color','k')
                    legend('channel 1', 'channel 2', 'channel 3', 'human', 'automated');
        %             plot(t(inds), tmp,'k')
        %             plot(t(inds), smoothed(inds),'g')
                    set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
%                     if playAudio == 1
%                         t0 = min(output{I(k),3},newStart) - 2; 
%                         t1 = max(output{I(k),3},newStart) + 2;
%                         inds = find(t >= t0 & t < t1);
%                         tt = t(inds) - t(inds(1));
%                         fprintf('t0=%0.4f\n',t0);
%                         playAudioWithCrossHair(tt,sig(inds)./max(abs(sig(inds))),fs);
%                     end
                    pause
                    close 
                end
            end
        end
    end
end
save output.mat output





