function [phoneme, word] = read_textgrid(fn,varargin)
% transforms the textgrid file into a matlab structs that contain the
% the phone. 
% input:
% filename, file to be read
% varargin{1} = 1 => get the words "sil" and the "sp" spaces too. 
% example:
% fn = '/Users/widemann/Documents/Prosodylab-Aligner-master/data/myexp_1_1_1.TextGrid';
% [phoneme, word] = read_textgrid(fn)
get_sil_sp = 0;
phoneme = struct();
word = struct();
if nargin > 1
    get_sil_sp = varargin{1};
end
fid = fopen(fn,'r');
% cnt = 0;
grp = 1;
while 1
    tline = fgetl(fid);
%     cnt = cnt+1;
    v = strfind(tline,'intervals: size =');
    if ~isempty(v) && grp == 1
%         fprintf('%d\t%s\n',cnt,tline);
        sz = str2double(tline(v+18:end));
        for k = 1:sz
            tline = fgetl(fid);
            tline = fgetl(fid);
            v = strfind(tline,'=');
            phoneme(k).xmin = str2double(tline(v+1:end));
            tline = fgetl(fid);
            v = strfind(tline,'=');
            phoneme(k).xmax = str2double(tline(v+1:end));
            tline = fgetl(fid);
            v = strfind(tline,'"');
            phoneme(k).text = tline(v(1)+1:v(2)-1);
        end
        v = [];
        grp = 2; 
    end
    if ~isempty(v) && grp == 2
%         fprintf('%d\t%s\n',cnt,tline);
        sz = str2double(tline(v+18:end));
        cnt = 1;
        if get_sil_sp == 0
            for k = 1:sz
                tline = fgetl(fid);
                tline = fgetl(fid);
                v = strfind(tline,'=');
                xmin = str2double(tline(v+1:end));
                tline = fgetl(fid);
                v = strfind(tline,'=');
                xmax = str2double(tline(v+1:end));
                tline = fgetl(fid);
                v = strfind(tline,'"');
                text = tline(v(1)+1:v(2)-1);
                if ~(strcmpi(text,'sil') || strcmpi(text,'sp'))
                    word(cnt).xmin = xmin;
                    word(cnt).xmax = xmax;
                    word(cnt).text = text;
                    cnt = cnt + 1;
                end
            end
        elseif get_sil_sp == 1
            for k = 1:sz
                tline = fgetl(fid);
                tline = fgetl(fid);
                v = strfind(tline,'=');
                word(k).xmin = str2double(tline(v+1:end));
                tline = fgetl(fid);
                v = strfind(tline,'=');
                word(k).xmax = str2double(tline(v+1:end));
                tline = fgetl(fid);
                v = strfind(tline,'"');
                word(k).text = tline(v(1)+1:v(2)-1);
            end
        end
        v = [];
    end
    if ~ischar(tline), break, end
    %disp(tline)
end
fclose(fid);   


end





