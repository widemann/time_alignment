**Time-alignment for human speech**

Problem: There is a person speaking who is being recorded at 16KHz. This 
audio data is then time-aligned with brain EEG data. We would like to build 
a classifier that is able to predict which word was said based on the EEG 
data. However, to be able to do this, we first need to know the when each 
word was spoken. We would like to capture the beginning of each word down to 
10ms. 

The steps in the 

**demo.m** 

file should walk you though my approach to time aligning a
subset of the spoken words. The main idea is this, first use the prosody
tool developed here http://prosodylab.org/tools/aligner/ to roughly align
the data. There is no need to download the code, it's attached. 
Then we use our technique to determine the beginning of a word
for only a subset of the words. Note: in order for prosody to work HTK must 
be installed and on the path.