%% PURPOSE : Plot Dynamic Crosshairs along [0, x) Axis During Audio Playback
%
%%
% FUNCTION CALL : f_ui_crosshairs_x(h_axe, h_player, d_pos_x)
%
% FUNCTION ARGUMENTS :
%
% h_axe    : handle on current axe
% h_player : handle on player
% d_pos_x  : (optional), 0 seconds by default, initial x position of crosshairs
%
% EXAMPLES :
%
% >> f_ui_crosshairs_x(h_axe, h_player, 0);
%

%*****************************************************************************************
%
% FILE NAME : f_ui_crosshairs_x(h_axe, h_player, d_pos_x)
% 
% PURPOSE   : Plot Dynamic Crosshairs along [0, x) Axis During Audio Playback
%              
% AUTHOR    : Thierry Le Gall, System Audio/Acoustic Engineer, th-legall@orange.fr
%
% DEVELOPMENT HISTORY :
%
% Date           Name(s)       Version  Description
% -------------  ------------- -------  ------------------------------------------------
% Jun. 03, 2007  T. Le Gall        0.1  creation
% Jul. 09, 2007  T. Le Gall        0.2  code opt.
% Jul. 11, 2007  T. Le Gall        0.3  code opt. and formatting for published HTLM
%
% NOTES :
%
% - Argument 'eventStruct' is mandatory event if pointed unused by editor.
%
% CUSTOM FUNCTIONS USED :
%
% - f_update_crosshairs_x()
% 
%*****************************************************************************************

function f_ui_crosshairs_x(h_axe, h_player, d_pos_x)

%% Init. constants and default values 

C_TIMER_PERIOD = 1/30; % (s)

if nargin < 3
    d_pos_x = 0; % (s)
end

axes(h_axe); % set as current axe

%% Create and set properties of vertical crosshairs object 

v_lim_y = get(h_axe, 'YLim');

h_line = line([d_pos_x, d_pos_x], [v_lim_y(1), v_lim_y(2)]);
set(h_line, 'Color', [1 0 0], 'EraseMode' , 'normal');

h_text = text(d_pos_x + 0.1, v_lim_y(1) + 0.1, 0, strcat(num2str(d_pos_x, 2), ' s'));
set(h_text, 'Color', [1 0 0], 'EraseMode' , 'normal', 'Fontsize', 8);

set(h_player, 'UserData', [h_line, h_text, v_lim_y],...
              'TimerPeriod', C_TIMER_PERIOD,...           
              'TimerFcn', {@f_update_crosshairs_x, d_pos_x},...
              'StopFcn', {@f_update_crosshairs_x, d_pos_x});           
end


%%  Sub-Function for f_ui_crosshairs_x() - Update Crosshairs During Playback 
% called by audioplayer during playback (if audioplayer enabled)
function f_update_crosshairs_x(hco, eventStruct, d_pos_x)

    v_handles = get(hco, 'UserData'); % get handles in current object
        
    if (hco.isplaying)   % only do this if playback is in progress
        d_sample_position_x = get(hco, 'CurrentSample') / get(hco, 'SampleRate');
    else
        d_sample_position_x = d_pos_x;
    end
    
    set(v_handles(1), 'XData', [d_sample_position_x, d_sample_position_x],...
                'YData', [v_handles(3), v_handles(4)]);
                
    set(v_handles(2), 'Position', [d_sample_position_x + 0.1, v_handles(3) + 0.1],...
                'String', strcat(num2str(d_sample_position_x, 2), ' s'));

end
