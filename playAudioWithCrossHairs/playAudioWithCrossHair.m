% function playAudioWithCrossHair(t,yn,fs)
% Input: 
%   t, time axis
%   yn, is the audio signal 
%   fs, is the sampling frequency.
% output:
%   a figure plot that plays the audio signal with cross hairs. 
%   control-c to exit it. 
% Example:
%     take = 18;
%     mic = 1;
%     [y, fs, nbits] = DataReader(mic,take,1;
%     yn = y./max(abs(y));
%     dt = 1/fs;
%     t = dt*(0:numel(y)-1);
%     playAudioWithCrossHair(t,yn,fs)
% written by 
%   Thierry Le Gall, System Audio/Acoustic Engineer, th-legall@orange.fr


function playAudioWithCrossHair(t,yn,fs)

dt = 1/fs;
tt = dt*(0:numel(t)-1);
h_figure = figure;
plot(tt, yn);
xlabel('Time (s)')
ylabel('Amplitude')
s = sprintf('Audio Signal \n t0 = %0.4f\t t1 = %0.4f', t(1),t(end)); 
title(s)

h_axe = get(h_figure, 'CurrentAxes');
grid('on');

%% Create Audio Player 
d_sample_freq = fs;
h_player = audioplayer(yn, d_sample_freq);

    %% Play Signal with Dynamic Crosshair 
    % flag = 1;
    % while(flag) % for replay on any key press
    f_ui_crosshairs_x(h_axe, h_player, tt(1));
    play(h_player);
    % pause;
    %stop(h_player);
    % end

    yesNo = input('Play again? (y/n)\n','s');
    if strcmpi(yesNo,'y')
        playAudioWithCrossHair(t,yn,fs)
    else
        stop(h_player);
        close(h_figure);
    end
end


