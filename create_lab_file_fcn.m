function create_lab_file_fcn(fn,cell_str)
% cleans up the string, cell_str, and writes it to file, fn.
fid = fopen(fn,'w');
pat = '[A-Z'']';
for k = 1:numel(cell_str)
    str = cell_str{k};
    s = regexp(str,pat);
    if ~isempty(s)
        tmp_s = str(s);
        w = find(tmp_s=='''');
        if (w ~= length(tmp_s)-1) | (w ~= length(tmp_s)-1)
            tmp_s(w) = [];
        end
        if strcmpi(tmp_s,'UNCLEAR')
            tmp_s = '';
        end   
        fprintf(fid, '%s ', tmp_s);
    end
end
fclose(fid);
fprintf('%s written\n',fn);


end

